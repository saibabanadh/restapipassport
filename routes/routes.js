var express = require('express');
var router = express.Router();
var Product = require('../app/models/product.js');
var orderCtrl = require('../app/controllers/order.js');

//Products
Product.methods(['get', 'post', 'put', 'delete']);
Product.register(router, '/products');

//Orders
router.get('/orders', orderCtrl.getOrders);
router.post('/orders', orderCtrl.postOrders);
router.get('/orders/:id', orderCtrl.getOrder);
router.put('/orders/:id', orderCtrl.putOrder);
router.delete('/orders/:id', orderCtrl.deleteOrder);




module.exports = router;


