//mongodb
var mongoose = require('mongoose');

var userCtrl = require('../app/controllers/user.js');





module.exports = function (app) {

    //routes
    app.route('/register').post(userCtrl.register);
    app.route('/login').get(userCtrl.getLogin);
    app.route('/login').post(userCtrl.login);
    app.route('/user/:username').get(userCtrl.findUserByName);
    app.route('/forgot').post(userCtrl.forgot);
    app.route('/reset/:uuid').get(userCtrl.getUuid);
    app.route('/reset').post(userCtrl.reset);
    app.route('/logout').get(userCtrl.logout);
    app.route('/deleteuser').post(userCtrl.deleteuser);

};