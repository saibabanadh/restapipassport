var express = require('express');
var router = express.Router();

var isAuthenticated = function (req, res, next) {
	if (req.isAuthenticated())
		return next();
	res.status(401).send('Unauthorized');
}

module.exports = function(passport){

	router.post('/login', function(req, res, next) {
	  passport.authenticate('login', function(err, user, message) {
	    if (err) { 
	    	return next(err); 
	    }
	    if (!user) { 
	    	res.status(401).json({error:message});
	    }
	    
	    if(user) {
	      res.status(200).json({user:user, message:message});
	    }
	  })(req, res, next);
	});

	router.post('/signup', function(req, res, next) {
	  passport.authenticate('signup', function(err, user, message) {
	    if (err) { 
	    	return next(err); 
	    }
	    if (!user) { 
	    	res.status(401).json({error:message});
	    }
	    
	    if(user) {
	      res.status(200).json({user:user, message:message});
	    }
	  })(req, res, next);
	});

	
	/* Handle Logout */
	router.get('/logout', function(req, res) {
		console.log(req.user, " at /logout")
		req.logout();
		res.status(200).send('Logged Out successfully');;
	});

	return router;
}