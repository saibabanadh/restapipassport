var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var util = require('util');


//Security Dependencies
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var helmet = require('helmet');
var MongoStore      = require('connect-mongo')({session:expressSession});
var config = require('./config/config');

//DB
var mongoose = require('mongoose');
var port = process.env.PORT || 3001;
var options = {
  db: { 
  	native_parser: true 
  },
  server: { 
  	poolSize: 5 ,
  	socketOptions:{ 
  		keepAlive: 1 
  	}
  }
}
mongoose.connect(config.db, options);

//Middleware
app.use(express.static('./app/views/'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({'extended':true}));
app.use(bodyParser.json());
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization' );
  next();
});

//Security
app.use(helmet.xframe());  

app.use(expressSession({
  store: new MongoStore({
        url: config.db,
        collection : 'sessions' 
  }),
  name:'sessionID',
  secret: 'IamtheOnewhoisnotlikeOthers',
  cookie: {
    httpOnly: true,
    secure: true,
    maxAge: 2628000000
  },
  saveUninitialized: true,
  resave: true
}));

app.use(passport.initialize());
app.use(passport.session());


require('./app/passport/init')(passport);
app.use('/', require('./routes/passport')(passport));



app.use('/api/v1',ensureAuth,require('./routes/routes'));

app.use(function(req, res) {
    var err = new Error("requested url: "+req.url+ ' is Not Found');
    res.status(404).send(err);
});

process.on('uncaughtException', function(err) {
  console.log(err);
});

app.listen(port,function(){
	console.log("Server is running");
});

function ensureAuth(req,res,next){
  console.log(util.inspect(req, false, null));
   if(req.user){
      next();
    }
    else{  
      res.redirect(303,'/login');
  }

  // if(!req.user.authenticated){
  //   return res.status(401).send("Not Authenticated");    
  // }
  // return next();
}

