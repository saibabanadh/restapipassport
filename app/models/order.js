var restful = require('node-restful');

var mongoose = require('mongoose');

var orderSchema = new mongoose.Schema({
	value:Number,
	from:String,
	city:String
});

module.exports = mongoose.model('Order', orderSchema);