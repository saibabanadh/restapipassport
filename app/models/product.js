var restful = require('node-restful');
var mongoose = restful.mongoose;

var productSchema = new mongoose.Schema({
	name:String,
	price:Number,
	contact:String
});

module.exports = restful.model('Product', productSchema);
