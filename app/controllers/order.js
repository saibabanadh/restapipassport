var Order = require('../models/order.js');



exports.getOrders = function(req,res){
	Order.find({},function(error, orders){
		if(error){
			res.status(500).send(error);
		}
		res.status(200).json( {data:orders});
	});
};

exports.postOrders = function(req,res){
	var order = new Order();
	order.value = req.body.value;
	order.from = req.body.from;
	order.city = req.body.city;

	order.save(function(error){
		if(error){
			res.status(500).send(error);
		}
		res.status(201).json({message:"Order Created", data:order});
	});
};

exports.getOrder = function(req,res){
	Order.findById(req.params.id, function(error, order) {
		if(error){
			res.status(500).send(error);
		}
		res.status(200).json( {data:order});
	});
};

exports.putOrder = function(req, res) {
	Order.findById(req.params.id, function(err, order) {
		if (err)
			res.status(500).send(err);
		order.value = req.body.value;
		order.from = req.body.from;
		order.city = req.body.city;

    // Save the Order and check for errors
    order.save(function(err) {
    	if (err)
    		res.status(500).send(err);

    	res.status(200).json({message:"Order Updated", data:order});
    });
});
};

// Create endpoint /api/Orders/:Order_id for DELETE
exports.deleteOrder = function(req, res) {
  // Use the Order model to find a specific Order and remove it
  Order.findByIdAndRemove(req.params.id, function(err) {
  	if (err)
  		res.status(500).send(err);

  	res.status(200).json({ message: 'Order removed!' });
  });
};